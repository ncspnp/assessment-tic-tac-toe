# Frontend Engineer Assessment - Tic Tac Toe

![Tic Tac Toe](tic-tac-toe.jpg "Tic Tac Toe")

Build a classic Tic Tac Toe game with the following stack:

- React / React Native (if you are applying for Mobile role please use RN)
- Use Vite for React, and Expo for RN as template
- Use TypeScript (preferable)
- Optional inclusions
    - State mananegement (React Context, Redux etc)
    - Unit test (preferable)
    - Styling libs (SASS, Styled Components, CSS Modules, Tailwind etc)
    - UI Libraries (Material, AntD, Chakra etc)

The image above is only a reference, it is not necessary to look exactly like it.

Along with the submission would be great if you can provide your profiles of the following if applicable for our reference. It will help us tremendously to get to know you better.

- GitHub
- Code snippets/repo eg. codesandbox, codepen, stackblitz etc
- Coding challenges/certification sites eg. HackerRank, HackerEarth etc

## Submission requirements

- Please clearly state extra build and run requirements if any
- Please share the assignment via a PRIVATE git repository
- We want to observe your commit messages and the process, please commit module by module, instead of committing at one shot
- Please add users as stated in the email as collaborators
